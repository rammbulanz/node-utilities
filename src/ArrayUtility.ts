
/**
 * 
 * @param list 
 * @param keyRetriever 
 */
export function GroupBy<T>(list: Array<T>, keyRetriever: (item: T) => string): [Array<T>] {
    let result = {} as [Array<T>];
    list.forEach(x => {
        var k = keyRetriever(x);
        if (!(k in result))
            result[k] = [];
        result[k].push(x);
    });
    return result;
}